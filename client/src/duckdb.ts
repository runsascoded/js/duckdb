/**
 * Adapted from https://github.com/ilyabo/graphnavi / https://github.com/duckdb/duckdb-wasm/issues/1148
 */
import { AsyncDuckDB, DuckDBBundle, selectBundle, getJsDelivrBundles, createWorker, ConsoleLogger, DuckDBConfig, DuckDBAccessMode, AsyncDuckDBConnection } from "@duckdb/duckdb-wasm"
export const { READ_WRITE, } = DuckDBAccessMode
import Worker from 'web-worker'
import * as path from "path"
import { useEffect, useState } from "react"

const ENABLE_DUCK_LOGGING = false;

const SilentLogger = { log: () => {}, };

// TODO: shut DB down at some point?

type WorkerBundle = { bundle: DuckDBBundle, worker: Worker }

export async function nodeWorkerBundle(): Promise<WorkerBundle> {
    const DUCKDB_DIST = `node_modules/@duckdb/duckdb-wasm/dist`
    const bundle = await selectBundle({
        mvp: {
            mainModule: path.resolve(DUCKDB_DIST, './duckdb-mvp.wasm'),
            mainWorker: path.resolve(DUCKDB_DIST, './duckdb-node-mvp.worker.cjs'),
        },
        eh: {
            mainModule: path.resolve(DUCKDB_DIST, './duckdb-eh.wasm'),
            mainWorker: path.resolve(DUCKDB_DIST, './duckdb-node-eh.worker.cjs'),
        },
    });
    console.log("duckdb nodeWorkerBundle:", bundle)
    const mainWorker = bundle.mainWorker
    if (mainWorker) {
        const worker = new Worker(mainWorker);
        return { bundle, worker }
    } else {
        throw Error(`No mainWorker: ${mainWorker}`)
    }
}

export async function browserWorkerBundle(): Promise<WorkerBundle> {
    const allBundles = getJsDelivrBundles();
    const bundle = await selectBundle(allBundles);
    console.log("duckdb browserWorkerBundle:", bundle)
    const mainWorker = bundle.mainWorker
    if (mainWorker) {
        const worker = await createWorker(mainWorker)
        return { bundle, worker }
    } else {
        throw Error(`No mainWorker: ${mainWorker}`)
    }
}

// Global AsyncDuckDB instance
let dbPromise: Promise<AsyncDuckDB> | null = null

/**
 * Fetch global AsyncDuckDB instance; initialize if necessary
 */
export function getDuckDb(): Promise<AsyncDuckDB> {
    if (!dbPromise) {
        dbPromise = initDuckDb()
    }
    return dbPromise
}

export const DefaultConfig: DuckDBConfig = {
    path: ":memory:",
    accessMode: READ_WRITE,
    query: { castBigIntToDouble: true, }
}

/**
 * Initialize global AsyncDuckDB instance
 */
export async function initDuckDb(config: DuckDBConfig = DefaultConfig): Promise<AsyncDuckDB> {
    const { path } = config
    // const path = config.path ?? ":memory:"
    const fetchTimerKey = `duckdb-wasm fetch ${path}`
    console.time(fetchTimerKey)
    const { worker, bundle } = await (typeof window === 'undefined' ? nodeWorkerBundle() : browserWorkerBundle())
    console.timeEnd(fetchTimerKey)
    console.log("bestBundle:", bundle)
    const dbTimerKey = `duckdb-wasm instantiate ${path}`
    console.time(dbTimerKey);
    const logger = ENABLE_DUCK_LOGGING
        ? new ConsoleLogger()
        : SilentLogger;
    const db = new AsyncDuckDB(logger, worker);
    await db.instantiate(bundle.mainModule, bundle.pthreadWorker);
    await db.open({
        path,
        query: {
            castBigIntToDouble: true,
        },
    });
    console.timeEnd(dbTimerKey);
    return db
}

export function useDb(opts?: { path?: string }): { db: AsyncDuckDB, conn: AsyncDuckDBConnection } | null {
    const [ db, setDb ] = useState<AsyncDuckDB>()
    const [ conn, setConn ] = useState<AsyncDuckDBConnection>()
    useEffect(
        () => {
            initDuckDb(opts).then(db => {
                setDb(db)
                db.connect().then(conn => setConn(conn))
            })
        },
        []
    )
    return db && conn ? { db, conn } : null
}

/**
 * Run a query against the provided DuckDB instance, round-trip through JSON to obtain plain JS objects
 */
export async function runQuery<T>(
    db: AsyncDuckDB,
    query: string,
): Promise<T[]> {
    const conn = await db.connect()
    const result = await conn.query(query)
    const proxies = result.toArray()
    conn.close()
    // console.log("proxies:", proxies)
    return proxies
}

/**
 * React "hook" wrapper around `runQuery`, for use on the client, and which expects an initial fetch of data (to be
 * loaded on the server and passed to the client as "props")
 */
export function useQuery<T>({ db, query, init }: { db: AsyncDuckDB | null, query: string | null, init: T[] }): T[] {
    const [ data, setData ] = useState<T[]>(init)
    useEffect(
        () => {
            if (!db || !query) return
            runQuery<T>(db, query).then(data => setData(data))
        },
        [ db, query, ]
    )
    return data
}

/**
 * Load a parquet file from a local path or URL
 */
export async function loadParquet<T>(path: string): Promise<T[]> {
    const db = await getDuckDb()
    return runQuery(db, `select * from read_parquet('${path}')`)
}

/**
 * Hook for loading a parquet file or URL; starts out `null`, gets populated asynchronously
 */
export function useParquet<T>(url?: string): T[] | null {
    const [ data, setData ] = useState<T[] | null>(null)
    useEffect(
        () => {
            if (!url) return
            loadParquet<T>(url).then(data => setData(data))
        },
        []
    )
    return data
}

/**
 * Convert [a byte array representing a Parquet file] to an array of records
 */
export async function parquetBuf2json<T>(bytes: number[] | Uint8Array, table: string): Promise<T[]> {
    const db = await getDuckDb()
    const uarr = new Uint8Array(bytes)
    await db.registerFileBuffer(table, uarr)
    return runQuery(db, `SELECT * FROM parquet_scan('${table}')`)
}

/**
 * Hook for converting a Parquet byte array to records
 */
export function useParquetBuf<T>(bytes: number[] | Uint8Array, table: string): T[] | null {
    const [ data, setData ] = useState<T[] | null>(null)
    useEffect(
        () => {
            parquetBuf2json<T>(bytes, table).then(data => setData(data))
        },
        []
    )
    return data
}
