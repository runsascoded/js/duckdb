import { AsyncDuckDB, DuckDBConfig } from "@duckdb/duckdb-wasm";
import { DefaultConfig, initDuckDb, runQuery } from "./duckdb";

export default class Db {
    db: Promise<AsyncDuckDB>
    config: DuckDBConfig

    constructor(config: DuckDBConfig = DefaultConfig) {
        console.log("rdub/duckdb/client: instantiating Db")
        this.config = config
        this.db = initDuckDb(config)
    }

    async query<T>(query: string): Promise<T[]> {
        return runQuery(await this.db, query)
    }

    async close() {
        return (await this.db).terminate()
    }
}
