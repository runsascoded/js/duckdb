# @rdub/duckdb-wasm
[DuckDB-Wasm][duckdb-wasm] utilities for [React] apps

[![@rdub/duckdb-wasm on NPM](https://img.shields.io/npm/v/@rdub/duckdb-wasm.svg?label=@rdub/duckdb-wasm)](https://npmjs.org/package/@rdub/duckdb-wasm)

## API
- [db.ts]: `Db` class
- [duckdb.ts]: `initDuckDb`, `runQuery`

## Examples

### [crashes.hudcostreets.org]
[Demo][demo]: plot constructed from two remote Parquet files:

[![](../server/screenshots/plot.gif)][demo]

[Source][`useCsvTable` call]; see also [@rdub/next-plotly]

### [iRe/www][iRe/www] (interactive reports for Jupyter notebooks):

![](../server/screenshots/table.gif)


[duckdb-wasm]: https://github.com/duckdb/duckdb-wasm/
[duckdb.js]: https://www.npmjs.com/package/duckdb
[React]: https://reactjs.org/
[duckdb-node#15]: https://github.com/duckdb/duckdb-node/issues/15#issuecomment-1815710645

[duckdb.ts]: src/duckdb.ts
[db.ts]: src/duckdb.ts

[crashes.hudcostreets.org]: https://crashes.hudcostreets.org
[source]: https://github.com/search?q=repo%3Ahudcostreets%2Fnj-crashes%20%40rdub%2Fduckdb&type=code
[demo]: https://crashes.hudcostreets.org/#per-year

[iRe/www]: https://gitlab.com/runsascoded/ire/www/-/tree/static?ref_type=heads
[`openTable`]: https://gitlab.com/runsascoded/ire/www/-/blob/d9b6369a0bb51698240be38c04b0835a2d4761bf/src/table.ts#L50-88

[`useCsvTable` call]: https://github.com/hudcostreets/nj-crashes/blob/512e7cc79c1fb799f7e97b764d77a3f0645f6533/www/src/njsp/plot.tsx#L210-L216

[@rdub/next-plotly]: https://gitlab.com/runsascoded/js/next-plotly
