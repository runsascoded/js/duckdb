# @rdub/duckdb
DuckDB+Node.js utilities

[![@rdub/duckdb on NPM](https://img.shields.io/npm/v/@rdub/duckdb.svg?label=@rdub/duckdb)](https://npmjs.org/package/@rdub/duckdb)

## API
- [db.ts]: `Db` class (implemented with [duckdb-async], falling back to [duckdb-lambda-x86]; see [duckdb-node#15])
- [duckdb.ts]: `initDuckDb`, `runQuery`

## Examples

### [iRe/www][iRe/www] (interactive reports for Jupyter notebooks):

![](screenshots/table.gif)

## Use with [Next.js]
Inspired by [next.js#46493] / [node.bcrypt.js#979], you probably want this Webpack config in order to access [duckdb-lambda-x86] from a Next.js app:

```diff
 /** @type {import('next').NextConfig} */
 const nextConfig = {
   …,
+  webpack: (config) => {
+    config.externals = [...config.externals, 'duckdb', 'duckdb-lambda-x86'];
+    return config;
+  },
 };
 
 module.exports = nextConfig;
```

Otherwise you'll see errors like:

```
./node_modules/.pnpm/@mapbox+node-pre-gyp@1.0.11/node_modules/@mapbox/node-pre-gyp/lib/util/nw-pre-gyp/index.html Module parse failed: Unexpected token (1:0)
```

[duckdb]: https://duckdb.org/
[duckdb-wasm]: https://github.com/duckdb/duckdb-wasm/
[duckdb.js]: https://www.npmjs.com/package/duckdb
[duckdb-async]: https://www.npmjs.com/package/duckdb-async
[duckdb-lambda-x86]: https://www.npmjs.com/package/duckdb-lambda-x86
[React]: https://reactjs.org/
[duckdb-node#15]: https://github.com/duckdb/duckdb-node/issues/15#issuecomment-1815710645

[Next.js]: https://nextjs.org/
[next.js#46493]: https://github.com/vercel/next.js/issues/46493#issuecomment-1446525651
[node.bcrypt.js#979]: https://github.com/kelektiv/node.bcrypt.js/issues/979#issuecomment-1446526365

[duckdb.ts]: src/duckdb.ts
[db.ts]: src/query.ts

[crashes.hudcostreets.org]: https://crashes.hudcostreets.org
[source]: https://github.com/search?q=repo%3Ahudcostreets%2Fnj-crashes%20%40rdub%2Fduckdb&type=code
[demo]: https://crashes.hudcostreets.org/#per-year

[iRe/www]: https://gitlab.com/runsascoded/ire/www/-/tree/static?ref_type=heads
[`openTable`]: https://gitlab.com/runsascoded/ire/www/-/blob/d9b6369a0bb51698240be38c04b0835a2d4761bf/src/table.ts#L50-88

[`makeTable`]: https://gitlab.com/runsascoded/ire/www/-/blob/d9b6369a0bb51698240be38c04b0835a2d4761bf/src/table.ts#L110-133
[`makeTable` call]: https://gitlab.com/runsascoded/ire/www/-/blob/d9b6369a0bb51698240be38c04b0835a2d4761bf/src/report.ts#L128
[`tablePage`]: https://gitlab.com/runsascoded/ire/www/-/blob/d9b6369a0bb51698240be38c04b0835a2d4761bf/src/table.ts#L90-108
[`tablePage` call]: https://gitlab.com/runsascoded/ire/www/-/blob/d9b6369a0bb51698240be38c04b0835a2d4761bf/components/data-tables/data-table.tsx#L71-77

[Plotly]: https://plotly.com/javascript/

[`useCsvTable` call]: https://github.com/hudcostreets/nj-crashes/blob/512e7cc79c1fb799f7e97b764d77a3f0645f6533/www/src/njsp/plot.tsx#L210-L216
[`useTable` call]: https://github.com/hudcostreets/nj-crashes/blob/512e7cc79c1fb799f7e97b764d77a3f0645f6533/www/src/njsp/plot.tsx#L253

[@rdub/next-plotly]: https://gitlab.com/runsascoded/js/next-plotly
