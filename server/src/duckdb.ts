// Wrapper around duckdb-async or duckdb-lambda-x86; some hosts / GLIBC versions only work with one or the other (e.g.
// my M1 Macbook only works with duckdb-async, but my development VM only works with duckdb-lambda-x86.)
//
// See also: https://github.com/duckdb/duckdb-node/issues/15#issuecomment-1815710645
import type { Database } from "duckdb-async"
import { OPEN_READWRITE } from "duckdb"

export type AccessMode = number | Record<string, string>
export type Replacer = (this: any, key: string, value: any) => any
export type Config = {
    path?: string
    accessMode?: AccessMode
    replacer?: Replacer
}

let create: (path: string, accessMode?: number | Record<string, string>) => Promise<Database>

export async function getCreate() {
    if (!create) {
        try {
            const duckdb = await import("duckdb-async")
            console.log("Loaded duckdb-async")
            create = duckdb.Database.create
        } catch (error) {
            console.log("duckdb error importing duckdb-async, attempting duckdb-lambda-x86", error)
            const duckdb = await import("duckdb-lambda-x86")
            console.log("Loaded duckdb-lambda-x86")
            create = duckdb.Database.create
        }
    }
    return create
}

export async function initDuckDb({ path = ":memory:", accessMode = OPEN_READWRITE }: Omit<Config, 'replacer'> = {}): Promise<Database> {
    const create = await getCreate()
    return create(path, accessMode)
}

/**
 * Run a query against the provided DuckDB instance, round-trip through JSON to obtain plain JS objects
 */
export async function runQuery<T>(
    db: Database,
    query: string,
    replacer?: Replacer,
): Promise<T[]> {
    const connection = await db.connect()
    return new Promise((resolve, reject) => {
        connection.all(query, (err: any, res: any) => {
            connection.close()
            if (err) {
                reject(err)
            } else {
                if (replacer) {
                    res = JSON.parse(JSON.stringify(res, replacer))
                }
                resolve(res)
            }
        })
    })
}
