import { Config, initDuckDb, runQuery } from "./duckdb"
import { Database } from "duckdb-async"
import { OPEN_READWRITE } from "duckdb"

export default class Db {
    db: Promise<Database>
    config: Config

    constructor({ path = ":memory:", accessMode = OPEN_READWRITE, replacer }: Config = {}) {
        this.db = initDuckDb({ path, accessMode })
        this.config = { path, accessMode, replacer }
    }

    async query<T>(query: string): Promise<T[]> {
        return runQuery(await this.db, query, this.config.replacer)
    }

    async close(clientOnly?: boolean): Promise<void> {
        if (clientOnly === true) return Promise.resolve()
        return (await this.db).close();
    }
}
