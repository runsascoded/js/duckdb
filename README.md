# [DuckDB][duckdb]+JS utilities

## @rdub/duckdb-wasm ([client](client))

[![@rdub/duckdb-wasm on NPM](https://img.shields.io/npm/v/@rdub/duckdb-wasm.svg?label=@rdub/duckdb-wasm)](https://npmjs.org/package/@rdub/duckdb-wasm)

## @rdub/duckdb ([server](server))

[![@rdub/duckdb on NPM](https://img.shields.io/npm/v/@rdub/duckdb.svg?label=@rdub/duckdb)](https://npmjs.org/package/@rdub/duckdb)

[duckdb]: https://duckdb.org/
